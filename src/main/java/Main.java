import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {


/*
                ****GET USERS****
 */
//        String url = "http://127.0.0.1:9090/users";
//        HttpClient client = HttpClient.newHttpClient();
//        HttpRequest request = HttpRequest.newBuilder()
//                .uri(URI.create(url))
//                .GET()
//                .build();
//        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
//        System.out.println("Contacts: " + response.body());
/*
                ********
 */



        /*
         ****LOGIN****
         */
        String url = "http://127.0.0.1:9090/login";
        String requestBody = "{\"login\":\"user\",\"password\":\"password\"}";

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
        /*
         *********
         */


        /*
         ****REGISTRATION****
         */
//        String url = "https://127.0.0.1:9090/register";
//        String requestBody = "{\"login\":\"user\",\"password\":\"password\",\"date_born\":\"1991-03-05\"}";
//
//        HttpClient client = HttpClient.newHttpClient();
//        HttpRequest request = HttpRequest.newBuilder()
//                .uri(URI.create(url))
//                .header("Accept", "application/json")
//                .header("Content-Type", "application/json")
//                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
//                .build();
//
//        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
//        System.out.println("Response code: " + response.statusCode());
//        System.out.println("Response body: " + response.body());
        /*
         *********
         */

    }
}
